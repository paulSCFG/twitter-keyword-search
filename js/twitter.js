(function ($, undefined){


	$('form#q').on('submit', function (e){
		var query = $('#name').val();
		
		fetch(query);		

		setInterval(function (){
			fetch(query);
		}, 5000);

		return false;
	});

	function fetch(query){
		$.ajax({
					url: 'http://search.twitter.com/search.json?',
					data: { q: query },
					dataType: 'jsonp',
					success: function (data) {
						var p = $.map(data.results, function (tw) {
							return $('<p></p>').text(tw.text)[0];
						});

						$('#tweets').html(p);
						$('#tweets p').each(function (tag) {
							var t = $(this).text();
							
							var newStr = highlight(query, t);
							$(this).html(newStr);
						});

						$('#tweets p').append('<hr />');

					}
				});
	}


	//Highlights a given word in a string
	function highlight(word, str){
		word = "(\\b"+ 
	   		 word.replace(/([{}()[\]\\.?*+^$|=!:~-])/g, "\\$1")
	    	+ "\\b)";

		var re = new RegExp(word, 'igm');
		var newStr = str.replace(re, "<span class='hi'>$1</span>");

		return newStr;
	}

})(jQuery);